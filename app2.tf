module "app2" {
    source              = "./modules/as3_http"
    name                = "app2"
    virtualIP           = "10.1.10.43"
    serverAddresses     = ["10.1.20.21"]
    servicePort         = 30880
    partition           = "prod"
    providers = {
      bigip = bigip.dmz
    }    
}
