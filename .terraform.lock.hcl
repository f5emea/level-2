# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/f5networks/bigip" {
  version     = "1.22.2"
  constraints = "1.22.2"
  hashes = [
    "h1:IMEoZLvA2dehZQ0xN2sYDzSHeOep6TAxyMO8DRG/VWU=",
    "zh:07fcb96eb9b56554c655e485939f6610fb4bd6af4cabbdc69454dee0650091b0",
    "zh:0a7c4518939f3ecebdf21d801179a209515fd367f2d2d0d9ef6c05dbfe453c84",
    "zh:3e2f1edb0d460c6a701e00c26a69d36434027d65de46752a07bcb34146526177",
    "zh:4405339743ae3d6db76103a4b363214cb1bc87268ca88b98b6520a79651e594a",
    "zh:4a29583be97972f69b1b930926b6992ccd45037f90b832c97bd3ce89ac3c151e",
    "zh:4f4839c29d92f824a260de305124f0af89cb8c58135ec7c1021ae9d3660b6c4e",
    "zh:616ed8869f49e2ad4a776223b21c36c60a72c1326b33d914302ac5189d1dca89",
    "zh:646975680ec5c9371a066d1188830c438b3b2f774a1cdf6123b8ca2e4a287aa9",
    "zh:9e094aeee8d80426c19f194c93c6e77b9be4bb24fc7e5b385d46bfa9c1d6d2de",
    "zh:b7fe6223b82fc4bc2455d539a6a35ca123bbb930f8c69a8b927764ea4414fbd5",
    "zh:c53c5008a046c4a4ec9ff9f23f089f1e4d7460c19564bdf9e8d1b1b0fde499e7",
    "zh:cbce6bfd9543334142c8c64c103e3d8e5aabaa96e14c9c03b12b0e429c6f61db",
    "zh:e1f47930dd3d57575b6a38d3b89f140db7cff69e1c9790b62255998e7d654800",
    "zh:f182b107f515f5a4ce6238dda78b73f95bb82472577a9a1243f227b64ea41e45",
  ]
}
